//@flow
const
  q = require('q'),
  _ = require('lodash'),
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { expect } = require('chai'),
  WebSocket = require('ws'),
  axios = require('axios'),
  debug = require('debug')('test:load'),

  { DisNode } = require('@cern/dim'),
  { DisProxy } = require('../src'),

  { createApp, waitEvent } = require('./utils');

/*::
import type { ex$App } from './utils'
*/

describe('DisProxy (load)', function() {
  var env /*: {
    dis: DisNode,
    app: ex$App,
    server: http$Server,
    url: string
  } */ = {};

  beforeEach(function() {
    env = {};

    env.dis = new DisNode('127.0.0.1', 0);

    return env.dis.listen()
    .then(() => createApp(env))
    .then(() => {
      DisProxy.register(env.app);
      /* eslint-disable-next-line max-len */
      env.url = `http://127.0.0.1:${env.server.address().port}/${env.dis.info.name}:${env.dis.info.port}/dis/subscribe`;
    });
  });

  afterEach(function() {
    env.dis.close();
    env.server.close();
    env = {};
  });

  it('can request a service a lot (ws + remote close)', function() {
    this.timeout(60000);
    const total = 1000;

    var srv = env.dis.addService('/TEST', 'I', 42);
    if (!srv) { throw 'failed to register service'; }

    function loopRequest(i /*: number */) {
      if (i <= 0) { return undefined; }

      if (i % 100 === 0) { debug('loop %i/%i', i, total); }
      var ws = new WebSocket(env.url);
      ws.once('open', () => {
        ws.send(JSON.stringify({
          name: '/TEST', definition: 'I', sid: 1, watch: true
        }));
      });
      return q()
      .then(_.constant(waitEvent(ws, 'message', 1)))
      .then((ret) => JSON.parse(ret.toString()))
      .then((ret) => expect(ret).to.contain({ value: 42 }))
      .then(() => ws.close(1000, 'leaving'))
      .then(() => loopRequest(i - 1));
    }
    return loopRequest(total);
  });

  it('can request a service a lot (axios stream + remote close)', function() {
    this.timeout(60000);
    const total = 2;
    const params = {
      name: '/TEST', definition: 'I', watch: true, stamped: false
    };

    var srv = env.dis.addService('/TEST', 'I', 42);
    if (!srv) { throw 'failed to register service'; }

    function loopRequest(i /*: number */) {
      if (i <= 0) { return undefined; }

      if (i % 100 === 0) { debug('loop %i/%i', i, total); }
      return q()
      .then(() => axios.get(env.url, { params, responseType: 'stream' }))
      .then((res) => {
        _.defer(res.data.resume.bind(res.data));
        return q()
        .then(_.constant(waitEvent(res.data, 'data')))
        .then((ret) => JSON.parse(ret.toString()))
        .tap((ret) => expect(ret).to.contain({ value: 42 }))
        .then(() => res.data.destroy())
        .then(() => res.request.removeAllListeners())
        .then(_.constant(waitEvent(res.data, 'close')));
      })
      .then(() => loopRequest(i - 1));
    }
    return loopRequest(total);
  });

});
