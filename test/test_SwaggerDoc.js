// @ts-check
const
  { get } = require('lodash'),
  { expect } = require('chai'),
  { describe, it, beforeEach, afterEach } = require('mocha'),
  axios = require('axios'),

  { register } = require('..'),
  { createApp } = require('./utils');

describe('SwaggerDoc', function() {
  var env /*: {
    dns: DnsServer,
    app: ex$App,
    server: http$Server,
    url: string
  } */ = {};

  beforeEach(async function() {
    env = {};

    await createApp(env);
    register(env.app);
    env.url =  `http://127.0.0.1:${env.server.address().port}/api-docs`;
  });

  afterEach(function() {
    env.server.close();
    env = {};
  });

  it('retrieve the API doc', async function() {
    const ret = await axios.get(env.url);
    expect(ret.data).to.contain('ReDIM-DIM API');
  });

  it('retrieve the API doc json', async function() {
    const ret = await axios.get(env.url + '.json');
    expect(get(ret, [ 'data', 'paths' ])).to.have.property('/{server}/dns/query');
  });
});
