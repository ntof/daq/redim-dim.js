//@flow
const
  express = require('express'),
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { expect } = require('chai'),
  WebSocket = require('ws'),

  { DnsServer } = require('@cern/dim'),
  { DnsProxy } = require('../src'),

  { createApp } = require('./utils');

/*::
import 'express'
import type { ex$App } from './utils'
*/

describe('DnsProxy (WebSocket)', function() {
  var env /*: {
    dns: DnsServer,
    app: ex$App,
    server: http$Server,
    url: string
  } */ = {};

  beforeEach(function() {
    env = {};

    env.dns = new DnsServer('127.0.0.1', 0);
    return env.dns.listen()
    .then(() => createApp(env))
    .then(() => {
      var router = express.Router();
      DnsProxy.register(router);
      env.app.use('/path', router);
      /* eslint-disable-next-line max-len */
      env.url = `http://127.0.0.1:${env.server.address().port}/path/127.0.0.1:${env.dns._conn.port}/dns/query`;
    });
  });

  afterEach(function() {
    env.dns.close();
    env.server.close();
    env = {};
  });

  it('can query a service', function(done) {
    var ws = new WebSocket(env.url);
    ws.once('open', () => {
      ws.send(JSON.stringify({ name: 'DIS_DNS/SERVER_LIST', sid: 1 }));
    });
    ws.once('message', (data) => {
      if (!data) { return; }
      var msg = JSON.parse(data);
      expect(msg).to.have.property('task');
      expect(msg).to.have.property('sid', 1);
      ws.once('close', () => { ws.removeAllListeners(); done(); });
      ws.close(1000, 'done');
    });
  });
});
