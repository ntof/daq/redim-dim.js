//@flow
const
  q = require('q'),
  _ = require('lodash'),
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { expect } = require('chai'),
  WebSocket = require('ws'),

  { DisNode } = require('@cern/dim'),
  { DisProxy } = require('../src'),

  { createApp, waitEvent } = require('./utils');

/*::
import type { ex$App } from './utils'
*/

describe('DisProxy (WebSocket)', function() {
  var env /*: {
    dis: DisNode,
    app: ex$App,
    server: http$Server,
    url: string
  } */ = {};

  beforeEach(function() {
    env = {};

    env.dis = new DisNode('127.0.0.1', 0);

    return env.dis.listen()
    .then(() => createApp(env))
    .then(() => {
      DisProxy.register(env.app);
      /* eslint-disable-next-line max-len */
      env.url = `http://127.0.0.1:${env.server.address().port}/${env.dis.info.name}:${env.dis.info.port}/dis/subscribe`;
    });
  });

  afterEach(function() {
    env.dis.close();
    env.server.close();
    env.app.removeAllListeners();
    env = {};
  });

  it('can manage keepalive packets', function(done) {
    var ws = new WebSocket(env.url);
    ws.once('open', () => {
      ws.send(JSON.stringify({ keepAlive: true }));
      ws.once('close', () => { ws.removeAllListeners(); done(); });
      ws.close(1000, 'done');
    });
    ws.once('message', () => {
      expect.fail(null, null, 'Should not receive a message with KeepAlive!');
    });
  });

  it('can retrieve service', function(done) {
    env.dis.addService('/TEST', 'I', 42);
    var ws = new WebSocket(env.url);

    ws.once('open', () => {
      ws.send(JSON.stringify({ name: '/TEST', definition: 'I', sid: 1 }));
    });
    ws.once('message', (data) => {
      if (!data) { return; }
      expect(JSON.parse(data)).to.deep.equal(
        { name: "/TEST", sid: 1, value: 42 });
      ws.once('close', () => { ws.removeAllListeners(); done(); });
      ws.close(1000, 'done');
    });
  });

  it('fails to retrieve unknown service', function(done) {
    var msgs = [];

    var ws = new WebSocket(env.url);
    ws.once('open', () => {
      ws.send(JSON.stringify({ name: '/TEST', definition: 'I', sid: 1 }));
    });
    ws.on('message', (data) => msgs.push(data));
    ws.once('close', () => {
      expect(msgs).to.be.empty();
      ws.removeAllListeners();
      done();
    });
  });

  it('can watch a service', function() {
    var srv = env.dis.addService('/TEST', 'I', 42);
    if (!srv) { throw 'failed to register service'; }

    var ws = new WebSocket(env.url);
    ws.once('open', () => {
      ws.send(JSON.stringify({
        name: '/TEST', definition: 'I', sid: 1, watch: true
      }));
    });
    return q()
    .then(_.constant(waitEvent(ws, 'message', 1)))
    .then((ret) => JSON.parse(ret.toString()))
    .then((ret) => expect(ret).to.contain({ value: 42 }))

    .then(() => srv.setValue(24))
    .then(_.constant(waitEvent(ws, 'message', 2)))
    .then((ret) => JSON.parse(ret[1].toString()))
    .then((ret) => expect(ret).to.contain({ value: 24 }))

    .then(() => ws.close(1000, 'leaving'))
    .then(_.constant(waitEvent(ws, 'close', 1)));
  });

  it('can unsubscribe as service', function() {
    var srv = env.dis.addService('/TEST', 'I', 42);
    if (!srv) { throw 'failed to register service'; }

    var ws = new WebSocket(env.url);
    ws.once('open', () => {
      ws.send(JSON.stringify({
        name: '/TEST', definition: 'I', sid: 1, watch: true
      }));
    });
    return q()
    .then(_.constant(waitEvent(ws, 'message', 1)))
    /* unsubscribe */
    .then(() => ws.send(JSON.stringify({ name: '/TEST', sid: 1,
      watch: false, initial: false, timer: 0 })))
    .delay(150) /* leave some time on the server side */

    .then(() => ws.send(JSON.stringify({ name: '/TEST', sid: 2,
      watch: true, definition: 'I' })))
    .then(_.constant(waitEvent(ws, 'message', 2)))
    .tap((ret) => expect(ret.length).to.equal(2))
    .then((ret) => JSON.parse(ret[1].toString()))
    .then((ret) => expect(ret).to.contain({ sid: 2 }))

    .then(() => ws.close(1000, 'leaving'))
    .then(_.constant(waitEvent(ws, 'close', 1)));
  });

  it('can poll a service', function() {
    this.timeout(5000);

    var stamps = [];
    var srv = env.dis.addService('/TEST', 'I', 42);

    if (!srv) { throw 'failed to register service'; }
    var ws = new WebSocket(env.url);
    ws.once('open', () => {
      ws.send(JSON.stringify({
        name: '/TEST', definition: 'I', sid: 1, timer: 1, stamped: true
      }));
    });
    return q()
    .then(_.constant(waitEvent(ws, 'message', 1)))
    .tap(() => stamps.push(_.now()))
    .then((ret) => JSON.parse(ret.toString()))
    .tap((ret) => expect(ret).to.contain({ value: 42 }))

    .then(() => srv.setValue(24))
    .then(_.constant(waitEvent(ws, 'message', 2, 4000)))
    .tap(() => stamps.push(_.now()))
    .then((ret) => JSON.parse(ret[1].toString()))
    .tap((ret) => expect(ret).to.contain({ value: 24 }))

    .then(() => expect(stamps[1] - stamps[0]).above(950).below(1050))
    .then(() => ws.close(1000, 'leaving'))
    .then(_.constant(waitEvent(ws, 'close', 1, 5000)));
  });

  it('can poll a service without initial value', function() {
    this.timeout(5000);

    var srv = env.dis.addService('/TEST', 'I', 42);
    var params = {
      name: '/TEST', definition: 'I', sid: 1, timer: 1, initial: false
    };

    if (!srv) { throw 'failed to register service'; }

    var startTime = _.now();
    var ws = new WebSocket(env.url);
    ws.once('open', () => ws.send(JSON.stringify(params)));
    return q()
    .then(_.constant(waitEvent(ws, 'message', 1, 2000)))
    .tap(() => expect(_.now() - startTime).to.be.within(950, 1050))
    .finally(() => ws.close(1000, 'leaving'));
  });

  it('fails to poll an unknown service', function() {
    const params = {
      name: '/UNKNOWN', definition: 'I', timer: 1, initial: true
    };
    var msgs = [];

    var ws = new WebSocket(env.url);
    ws.once('open', () => ws.send(JSON.stringify(params)));
    ws.on('message', (data) => msgs.push(data));
    return q()
    .then(_.constant(waitEvent(ws, 'close', 1, 2000)))
    .then(() => {
      expect(msgs).to.be.empty();
      ws.removeAllListeners();
    });
  });

  it('fails to poll an unknown server', function() {
    var msgs = [];
    const params = { name: '/TEST', definition: 'I', timer: 1, initial: true };

    /* eslint-disable-next-line max-len */
    var ws = new WebSocket(`http://127.0.0.1:${env.server.address().port}/unknown:1234/dis/subscribe`);
    ws.once('open', () => ws.send(JSON.stringify(params)));
    ws.on('message', (data) => msgs.push(data));
    return q()
    .then(_.constant(waitEvent(ws, 'close', 1, 2000)))
    .then((evt) => {
      expect(msgs).to.be.empty();
      expect(evt).to.equal(1001);
      ws.removeAllListeners();
    });
  });
});
