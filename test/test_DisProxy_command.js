//@flow
const
  q = require('q'),
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { expect } = require('chai'),
  axios = require('axios'),

  { DisNode } = require('@cern/dim'),
  { DisProxy } = require('../src'),

  { createApp } = require('./utils');

/*::
import 'express'
import type { ex$App } from './utils'
*/

describe('DisProxy', function() {
  var env /*: {
    dis: DisNode,
    app: ex$App,
    server: http$Server,
    url: string
  } */ = {};

  beforeEach(function() {
    env = {};

    env.dis = new DisNode('127.0.0.1', 0);

    return env.dis.listen()
    .then(() => createApp(env))
    .then(() => {
      DisProxy.register(env.app);
      /* eslint-disable-next-line max-len */
      env.url = `http://127.0.0.1:${env.server.address().port}/${env.dis.info.name}:${env.dis.info.port}/dis/command`;
    });
  });

  afterEach(function() {
    env.dis.close();
    env.server.close();
    env = {};
  });

  it('can call a command', function() {
    var values = [];
    var cmd = env.dis.addCmd('/CMD', 'I', (req) => values.push(req.value));
    const data = { name: '/CMD', definition: 'I', data: 44 };

    if (!cmd) { throw 'failed to register service'; }

    return q()
    .then(() => axios.post(env.url, data))
    .then((res) => expect(res.status).to.equal(204))
    .delay(200)
    .then(() => expect(values).to.deep.equal([ 44 ]));
  });

  it('fails if name is missing', function() {
    var values = [];
    var cmd = env.dis.addCmd('/CMD', 'I', (req) => values.push(req.value));
    const data = { definition: 'I', data: 44 };

    if (!cmd) { throw 'failed to register service'; }

    return q()
    .then(() => axios.post(env.url, data))
    .then(
      () => { throw 'should fail'; },
      (err) => expect(err).to.have.property('response').deep.contain(
        { status: 400, data: 'name argument missing' }));
  });

  it('can call an RPC', function() {
    var cmd = env.dis.addRpc('/RPC', 'I,I|RPC', (req) => req.value + 1);
    if (!cmd) { throw 'failed to register service'; }

    const data = {
      sid: 1, name: '/RPC/RpcIn', definition: 'I,I|RPC', data: 41
    };

    return q()
    .then(() => axios.post(env.url, data))
    .then((res) => expect(res.data).to.deep.equal(
      { sid: 1, name: '/RPC/RpcIn', value: 42 }));
  });
});
