//@flow
const
  q = require('q'),
  _ = require('lodash'),
  { expect } = require('chai'),
  debug = require('debug')('test'),
  express = require('express'),
  ews = require('express-ws');

/*::
  export type ex$Req = express$Request
  export type ex$Res = express$Response
  export type ex$App = express$Application<ex$Req, ex$Res>
 */

function createApp(env /*: { app: ex$App, server: http$Server } */,
                   port /*: ?number */) {
  var def = q.defer();
  env.app = express();
  ews(env.app);

  var server = env.app.listen(port || 0, function() {
    if (server) {
      env.server = server;
      def.resolve();
    }
    else {
      def.reject('failed to create Express server');
    }
  });
  /* $FlowIgnore: string argument is optional */
  env.app.use(function(req, res, next) {
    /* required by express-ws that doesn't properly remove its hooks */
    req.socket.once('close', () => {
      req.socket.removeAllListeners();
      req.removeAllListeners();
    });
    next();
  });
  return def.promise;
}

function waitEvent(obj /*: any */,
                   event /*: string */,
                   count /*: ?number */,
                   timeout /*: ?number */) /*: any */ {
  var def = q.defer();
  var ret = [];
  var received = 0;

  count = _.defaultTo(count, 1);
  timeout = _.defaultTo(timeout, 1000);

  var cb = function(e) {
    ret.push(e);
    debug('waitEvent: received:%s', event);
    /* $FlowIgnore */
    if (++received >= count) { def.resolve((count === 1) ? ret[0] : ret); }
  };
  obj.on(event, cb);
  return def.promise
  .timeout(timeout)
  .finally(function() {
    if (def.promise.isPending()) { def.reject(); }
    obj.removeListener(event, cb);
  });
}

/**
 * @param  {() => boolean|any} cb
 * @param {string} [msg]
 * @param  {number} [timeout=1000]
 * @return {Q.Promise<any>}
 */
function waitFor(cb, msg = undefined, timeout = 1000) {
  var def = q.defer();
  var resolved = false;
  var err = new Error(msg || 'timeout'); /* create this early to have stacktrace */
  var timer = _.delay(() => {
    resolved = true;
    clearTimeout(nextTimer);
    def.reject(err);
  }, _.defaultTo(timeout, 1000));
  var nextTimer = null;


  function next() {
    if (resolved) { return; }
    try {
      var ret = cb(); /* eslint-disable-line callback-return */
      if (ret) {
        clearTimeout(timer);
        resolved = true;
        def.resolve(ret);
      }
      else {
        nextTimer = _.delay(next, 10);
      }
    }
    catch (e) {
      clearTimeout(timer);
      resolved = true;
      def.reject(_.has(e, 'message') ? e : new Error(e));
    }
  }

  next();
  return def.promise;
}

function throws(fun) {
  try {
    fun();
    return null;
  }
  catch (e) {
    return e.message;
  }
}

/**
 * @template T=any
 * @param  {() => T} test
 * @param  {any} value
 * @param  {string} [msg]
 * @param  {number} [timeout]
 * @return {Q.Promise<T>}
 */
function waitForValue(test, value, msg = undefined, timeout = undefined) {
  /** @type {any} */
  var _val;
  return waitFor(() => {
    _val = test();
    return throws(() => expect(_val).to.deep.equal(value)) === null;
  }, msg, timeout)
  .catch((err) => {
    const msg = throws(expect(_val).to.deep.equal(value)) ||
      "Invalid result value: " + _.toString(_val) + " != " + value;
    if (err instanceof Error) {
      err.message = msg;
      throw err;
    }
    throw new Error(msg);
  });
}

module.exports = {
  createApp, waitEvent, waitFor, waitForValue
};
