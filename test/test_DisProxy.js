//@flow
const
  q = require('q'),
  _ = require('lodash'),
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { expect } = require('chai'),
  axios = require('axios'),

  { DisNode } = require('@cern/dim'),
  { DisProxy } = require('../src'),

  { createApp, waitEvent } = require('./utils');

/*::
import 'express'
import type { ex$App } from './utils'
*/

function parseReply(data) {
  /* eslint no-param-reassign:0*/
  if (typeof data === 'string') {
    try {
      data = JSON.parse(data);
    }
    catch (e) {
      return undefined;
    }
  }
  return data;
}

describe('DisProxy', function() {
  var env /*: {
    dis: DisNode,
    app: ex$App,
    server: http$Server,
    url: string
  } */ = {};

  beforeEach(function() {
    env = {};

    env.dis = new DisNode('127.0.0.1', 0);

    return env.dis.listen()
    .then(() => createApp(env))
    .then(() => {
      DisProxy.register(env.app);
      /* eslint-disable-next-line max-len */
      env.url = `http://127.0.0.1:${env.server.address().port}/${env.dis.info.name}:${env.dis.info.port}/dis/subscribe`;
    });
  });

  afterEach(function() {
    env.dis.close();
    env.server.close();
    env = {};
  });

  it('can retrieve service', function() {
    env.dis.addService('/TEST', 'I', 42);
    const params = { name: '/TEST', definition: 'I' };

    return q()
    .then(() => axios.get(env.url, { params }))
    .then((ret) => expect(ret.data).to.have.property('value').equal(42));
  });

  it('allows cors requests', function() {
    env.dis.addService('/TEST', 'I', 42);
    const params = { name: '/TEST', definition: 'I' };

    return q()
    .then(() => axios.get(env.url, { params,
      headers: { Origin: 'http://here' } }))
    .tap((ret) => expect(ret.headers).to.have.property(
      'access-control-allow-origin', '*'));
  });

  it('fails to retrieve unknown service', function() {
    const params = { name: '/TEST', definition: 'I' };

    return q()
    .then(() => axios.get(env.url,
      { params, transformResponse: [ parseReply ] }))
    .then(
      () => { throw 'should fail'; },
      (err) => expect(err).to.have.nested.property('response.status', 502));
  });

  it('can watch a service', function() {
    var srv = env.dis.addService('/TEST', 'I', 42);
    const params = {
      name: '/TEST', definition: 'I', watch: true, stamped: false
    };

    if (!srv) { throw 'failed to register service'; }

    return q()
    .then(() => axios.get(env.url,
      { params, responseType: 'stream' }))
    .then((res) => {
      _.defer(res.data.resume.bind(res.data));
      return q()
      .then(_.constant(waitEvent(res.data, 'data')))
      .then((ret) => JSON.parse(ret.toString()))
      .tap((ret) => expect(ret).to.contain({ value: 42 }))
      .tap((ret) => expect(ret).to.not.have.property('timestamp'))

      .then(() => srv.setValue(24))
      .then(_.constant(waitEvent(res.data, 'data', 2)))
      .then((ret) => JSON.parse(ret[1].toString()))
      .then((ret) => expect(ret).to.contain({ value: 24 }))

      .then(() => env.dis.removeService('/TEST'))
      .then(_.constant(waitEvent(res.data, 'data', 3)))
      .then((ret) => JSON.parse(ret[2].toString()))
      .then((ret) => expect(ret).to.contain({ value: null }))

      .then(() => res.data.destroy());
    });
  });

  it('closes when service terminates', function() {
    var srv = env.dis.addService('/TEST', 'I', 42);
    const params = {
      name: '/TEST', definition: 'I', watch: true, stamped: false
    };

    if (!srv) { throw 'failed to register service'; }

    return q()
    .then(() => axios.get(env.url,
      { params, responseType: 'stream' }))
    .then((res) => {
      _.defer(res.data.resume.bind(res.data));
      return q()
      .then(_.constant(waitEvent(res.data, 'data')))
      .then((ret) => JSON.parse(ret.toString()))
      .tap((ret) => expect(ret).to.contain({ value: 42 }))

      /* close the server side */
      .then(() => env.dis.close())
      .then(_.constant(waitEvent(res.data, 'end')))
      .then(() => res.data.destroy());
    });
  });

  it('can poll a service', function() {
    this.timeout(5000);

    var stamps = [];
    var srv = env.dis.addService('/TEST', 'I', 42);
    const params = {
      name: '/TEST', definition: 'I', timer: 1, watch: false, stamped: true
    };

    if (!srv) { throw 'failed to register service'; }

    return q()
    .then(() => axios.get(env.url,
      { params, responseType: 'stream' }))
    .then((res) => {
      return q()
      .then(_.constant(waitEvent(res.data, 'data', 1, 2000)))
      .tap(() => stamps.push(_.now()))
      .then((ret) => JSON.parse(ret.toString()))
      .tap((ret) => expect(ret).to.contain({ value: 42 }))
      .tap((ret) => expect(ret).to.have.property('timestamp'))

      .then(() => srv.setValue(24))
      .then(_.constant(waitEvent(res.data, 'data', 2, 4000)))
      .tap(() => stamps.push(_.now()))
      .then((ret) => JSON.parse(ret[1].toString()))
      .tap((ret) => expect(ret).to.contain({ value: 24 }))

      .then(() => expect(stamps[1] - stamps[0]).above(950).below(1050))
      .finally(() => res.data.destroy());
    });
  });

  it('can poll a service without initial value', function() {
    this.timeout(5000);

    var srv = env.dis.addService('/TEST', 'I', 42);
    const params = { name: '/TEST', definition: 'I', timer: 1, initial: false };

    if (!srv) { throw 'failed to register service'; }

    var startTime = _.now();
    return q()
    .then(() => axios.get(env.url, { params, responseType: 'stream' }))
    .then((res) => {
      _.defer(res.data.resume.bind(res.data));
      return q()
      .then(_.constant(waitEvent(res.data, 'data', 1, 2000)))
      .tap(() => expect(_.now() - startTime).to.be.within(950, 1050))
      .finally(() => res.data.destroy());
    });
  });

  it('fails to poll an unknown service', function() {
    const params = {
      name: '/UNKNOWN', definition: 'I', timer: 1, initial: true
    };

    return q()
    .then(() => axios.get(env.url, { params, responseType: 'stream' }))
    .then(
      () => { throw 'should fail'; },
      (err) => expect(err).to.have.nested.property('response.status', 502));
  });

  it('fails to poll an unknown server', function() {
    const params = { name: '/TEST', definition: 'I', timer: 1, initial: true };

    return q()
    /* eslint-disable-next-line max-len */
    .then(() => axios.get(`http://127.0.0.1:${env.server.address().port}/unknown:1234/dis/subscribe`, { params }))
    .then(
      () => { throw 'should fail'; },
      (err) => expect(err).to.have.nested.property('response.status', 502));
  });

  it('fails if name is missing', function() {
    const params = { definition: 'I', initial: true };

    return q()
    .then(() => axios.get(env.url, { params }))
    .then(
      () => { throw 'should fail'; },
      (err) => expect(err).to.have.property('response').deep.contain(
        { status: 400, data: 'name argument missing' }));
  });
});
