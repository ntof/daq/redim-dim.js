//@flow
const
  q = require('q'),
  _ = require('lodash'),
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { expect } = require('chai'),
  axios = require('axios'),

  { DnsServer, DisNode } = require('@cern/dim'),
  { DnsProxy } = require('../src'),

  { createApp, waitEvent } = require('./utils');

/*::
import 'express'
import type { ex$App } from './utils'
*/

describe('DnsProxy', function() {
  var env /*: {
    dns: DnsServer,
    app: ex$App,
    server: http$Server,
    url: string
  } */ = {};

  beforeEach(function() {
    env = {};

    env.dns = new DnsServer('127.0.0.1', 0);
    return env.dns.listen()
    .then(() => createApp(env))
    .then(() => {
      DnsProxy.register(env.app);
      /* eslint-disable-next-line max-len */
      env.url = `http://127.0.0.1:${env.server.address().port}/127.0.0.1:${env.dns._conn.port}/dns/query`;
    });
  });

  afterEach(function() {
    env.dns.close();
    env.server.close();
    env = {};
  });

  it('can query a service', function() {
    const params = { name: 'DIS_DNS/SERVER_LIST' };

    return q()
    .then(() => axios.get(env.url, { params }))
    .tap((ret) => expect(ret.data).to.have.property('task'));
  });

  it('allows cors requests', function() {
    return q()
    .then(() => axios.get(env.url, {
      params: { name: 'DIS_DNS/SERVER_LIST' },
      headers: { Origin: 'http://here' } }))
    .tap((ret) => expect(ret.headers).to.have.property(
      'access-control-allow-origin', '*'));
  });

  it('can query an unknown service', function() {
    const params = { name: 'UNKNOWN' };

    return q()
    .then(() => axios.get(env.url, { params }))
    .then((ret) => expect(ret.data).to.deep.equal({ node: null }));
  });

  it('can listen for service changes', function() {
    const params = { name: '/SERVICE', sid: 1 };
    var dis = new DisNode('127.0.0.1', 0);
    dis.addService('/SERVICE', 'I', 42);

    return q()
    .then(() => axios.get(env.url, { params, responseType: 'stream' }))
    .then((res) => {
      return q()
      .then(_.constant(waitEvent(res.data, 'data')))
      .then((ret) => JSON.parse(ret.toString()))
      .then((ret) => expect(ret).to.deep.equal({ node: null, sid: '1' }))
      .then(() => dis.register(env.dns.url() || ''))
      .then(_.constant(waitEvent(res.data, 'data', 2)))
      .then((ret) => JSON.parse(ret[1].toString()))
      .then((ret) => expect(ret).to.have.property('node', '127.0.0.1'))
      .then(() => res.data.destroy());
    })
    .finally(() => dis.close());
  });
});
