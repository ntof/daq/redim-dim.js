//@flow
const
  q = require('q'),
  _ = require('lodash'),
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { expect } = require('chai'),
  WebSocket = require('ws'),

  { DisNode } = require('@cern/dim'),
  { DisProxy } = require('../src'),

  { createApp, waitEvent } = require('./utils');

/*::
import type { ex$App } from './utils'
*/

describe('DisProxy (WebSocket)', function() {
  var env /*: {
    dis: DisNode,
    app: ex$App,
    server: http$Server,
    url: string
  } */ = {};

  beforeEach(function() {
    env = {};

    env.dis = new DisNode('127.0.0.1', 0);

    return env.dis.listen()
    .then(() => createApp(env))
    .then(() => {
      DisProxy.register(env.app);
      /* eslint-disable-next-line max-len */
      env.url = `http://127.0.0.1:${env.server.address().port}/${env.dis.info.name}:${env.dis.info.port}/dis/command`;
    });
  });

  afterEach(function() {
    env.dis.close();
    env.server.close();
    env = {};
  });

  it('can manage keepalive packets', function(done) {
    var ws = new WebSocket(env.url);
    ws.once('open', () => {
      ws.send(JSON.stringify({ keepAlive: true }));
      ws.once('close', () => { ws.removeAllListeners(); done(); });
      ws.close(1000, 'done');
    });
    ws.once('message', () => {
      expect.fail(null, null, 'Should not receive a message with KeepAlive!');
    });
  });

  it('can call a command', function() {
    var values = [];
    var cmd = env.dis.addCmd('/CMD', 'I', (req) => values.push(req.value));
    const data = { name: '/CMD', definition: 'I', data: 44 };

    if (!cmd) { throw 'failed to register service'; }

    var ws = new WebSocket(env.url);
    return q()
    .then(_.constant(waitEvent(ws, 'open', 1)))
    .then(() => {
      ws.send(JSON.stringify(data));
      ws.close(1000, 'done'); /* don't wait, just close it */
    })
    .then(_.constant(waitEvent(ws, 'close', 1, 2000)))
    .delay(100)
    .then((evt) => {
      expect(values).to.deep.equal([ 44 ]);
      expect(evt).to.equal(1000);
    });
  });

  it('can call several commands', function() {
    var values = [];
    var cmd = env.dis.addCmd('/CMD', 'I', (req) => values.push(req.value));
    if (!cmd) { throw 'failed to register service'; }

    var ws = new WebSocket(env.url);
    return q()
    .then(_.constant(waitEvent(ws, 'open', 1)))
    .then(() => ws.send(JSON.stringify(
      { name: '/CMD', definition: 'I', data: 44 })))
    .then(() => ws.send(JSON.stringify(
      { name: '/CMD', definition: 'I', data: 42 })))
    .delay(100)
    .then(() => ws.send(JSON.stringify(
      { name: '/CMD', definition: 'I', data: 41 })))
    .then(() => ws.close(1000, 'done'))
    .then(_.constant(waitEvent(ws, 'close', 1, 2000)))
    .then((evt) => {
      expect(values).to.deep.equal([ 44, 42, 41 ]);
      expect(evt).to.equal(1000);
    });
  });

  it('can call an RPC', function() {
    var cmd = env.dis.addRpc('/RPC', 'I,I|RPC', (req) => req.value + 1);
    if (!cmd) { throw 'failed to register service'; }

    var ws = new WebSocket(env.url);
    return q()
    .then(_.constant(waitEvent(ws, 'open', 1)))
    .then(() => ws.send(JSON.stringify(
      { sid: 1, name: '/RPC/RpcIn', definition: 'I,I|RPC', data: 44 })))
    .then(_.constant(waitEvent(ws, 'message', 1, 2000)))
    .then((msg) => {
      expect(JSON.parse(msg)).to.deep.contain({ value: 45, sid: 1 });
    })
    .then(() => ws.send(JSON.stringify(
      { sid: 2, name: '/RPC/RpcIn', definition: 'I,I|RPC', data: 42 })))
    .delay(100)
    .then(() => ws.send(JSON.stringify(
      { sid: 3, name: '/RPC/RpcIn', definition: 'I,I|RPC', data: 41 })))
    .then(_.constant(waitEvent(ws, 'message', 3, 2000)))
    .then(() => ws.close(1000, 'done'))
    .then(_.constant(waitEvent(ws, 'close', 1, 2000)))
    .then((evt) => {
      expect(evt).to.equal(1000);
    });
  });
});
