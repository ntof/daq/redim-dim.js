//@flow
const
  q = require('q'),
  _ = require('lodash'),
  { describe, it, beforeEach, afterEach } = require('mocha'),
  WebSocket = require('ws'),

  { DnsServer, DisNode } = require('@cern/dim'),
  { DnsProxy } = require('../src'),

  { createApp, waitEvent } = require('./utils');

/*::
import type { ex$App } from './utils'
*/

describe('DnsProxy (load)', function() {
  var env /*: {
    dns: DnsServer,
    app: ex$App,
    server: http$Server,
    url: string
  } */ = {};

  beforeEach(function() {
    env = {};

    env.dns = new DnsServer('127.0.0.1', 0);
    return env.dns.listen()
    .then(() => createApp(env))
    .then(() => {
      DnsProxy.register(env.app);
      /* eslint-disable-next-line max-len */
      env.url = `http://127.0.0.1:${env.server.address().port}/127.0.0.1:${env.dns._conn.port}/dns/query`;
    });
  });

  afterEach(function() {
    env.dns.close();
    env.server.close();
    env = {};
  });

  it('can request a lot of services (single ws)', function() {
    var dis = new DisNode('127.0.0.1', 0);
    dis.addService('/TEST', 'I', 42);
    var total = 200;

    var ws = new WebSocket(env.url);
    ws.once('open', () => {
      for (var i = 0; i < total; ++i) {
        ws.send(JSON.stringify({ name: '/TEST', sid: i + 1 }));
      }
    });
    return q()
    .then(_.constant(waitEvent(ws, 'message', total)))
    .then(() => ws.close(1000, 'leaving'))
    .then(_.constant(waitEvent(ws, 'close', 1)))
    .finally(() => dis.close());
  });
});
