//@flow
const
  q = require('q'),
  _ = require('lodash'),
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { expect } = require('chai'),
  WebSocket = require('ws'),

  { DnsServer, DisNode } = require('@cern/dim'),
  { DnsProxy } = require('../src'),

  { createApp, waitEvent, waitForValue } = require('./utils');

/*::
import type { ex$App } from './utils'
*/


describe('DnsProxy (WebSocket)', function() {
  var env /*: {
    dns: DnsServer,
    app: ex$App,
    server: http$Server,
    url: string
  } */ = {};

  beforeEach(function() {
    env = {};

    env.dns = new DnsServer('127.0.0.1', 0);
    return env.dns.listen()
    .then(() => createApp(env))
    .then(() => {
      DnsProxy.register(env.app);
      /* eslint-disable-next-line max-len */
      env.url = `http://127.0.0.1:${env.server.address().port}/127.0.0.1:${env.dns._conn.port}/dns/query`;
    });
  });

  afterEach(function() {
    env.dns.close();
    env.server.close();
    env = {};
  });

  it('can manage keepalive packets', function(done) {
    var ws = new WebSocket(env.url);
    ws.once('open', () => {
      ws.send(JSON.stringify({ keepAlive: true }));
      ws.once('close', () => { ws.removeAllListeners(); done(); });
      ws.close(1000, 'done');
    });
    ws.once('message', () => {
      expect.fail(null, null, 'Should not receive a message with KeepAlive!');
    });
  });

  it('can query a service', function(done) {
    var ws = new WebSocket(env.url);
    ws.once('open', () => {
      ws.send(JSON.stringify({ name: 'DIS_DNS/SERVER_LIST', sid: 1 }));
    });
    ws.once('message', (data) => {
      if (!data) { return; }
      var msg = JSON.parse(data);
      expect(msg).to.have.property('task');
      expect(msg).to.have.property('sid', 1);
      ws.once('close', () => { ws.removeAllListeners(); done(); });
      ws.close(1000, 'done');
    });
  });

  it('can refresh a query', async function() {
    const dis = new DisNode('127.0.0.1', 0);
    dis.addService('/TEST', 'C', '42');
    dis.register(env.dns.url() || '');

    const ws = new WebSocket(env.url);

    ws.once('open', () => {
      ws.send(JSON.stringify({ name: '/TEST', sid: 1 }));
    });
    try {
      const values = [];
      ws.on('message', (data) => {
        if (!data) { return; }
        var msg = JSON.parse(data);
        values.push(_.pick(msg, [ 'sid', 'definition', 'node' ]));
      });
      await waitForValue(() => values,
        [ { sid: 1, definition: 'C', node: '127.0.0.1' } ]);

      ws.send(JSON.stringify({ name: '/TEST', sid: 1 }));
      await waitForValue(() => values, [
        { sid: 1, definition: 'C', node: '127.0.0.1' },
        { sid: 1, definition: 'C', node: '127.0.0.1' }
      ]);

      dis.close();
      await waitForValue(() => values, [
        { sid: 1, definition: 'C', node: '127.0.0.1' },
        { sid: 1, definition: 'C', node: '127.0.0.1' },
        { sid: 1, node: null }
      ]);
      ws.send(JSON.stringify({ name: '/TEST', sid: 1 }));
      await waitForValue(() => values, [
        { sid: 1, definition: 'C', node: '127.0.0.1' },
        { sid: 1, definition: 'C', node: '127.0.0.1' },
        { sid: 1, node: null },
        { sid: 1, node: null }
      ]);
    }
    finally {
      dis.close();
      ws.close(1000, 'done');
      ws.removeAllListeners();
    }
  });

  it('can listen for service changes', function() {
    var dis = new DisNode('127.0.0.1', 0);
    dis.addService('/TEST', 'I', 42);

    var ws = new WebSocket(env.url);
    ws.once('open', () => {
      ws.send(JSON.stringify({ name: '/TEST', sid: 1 }));
    });
    return q()
    .then(_.constant(waitEvent(ws, 'message', 1)))
    .then((ret) => JSON.parse(ret.toString()))
    .then((ret) => expect(ret).to.contain({ node: null }))

    .then(() => dis.register(env.dns.url() || ''))
    .then(_.constant(waitEvent(ws, 'message', 2)))
    .then((ret) => JSON.parse(ret[1].toString()))
    .then((ret) => expect(ret).to.have.property('task').not.empty())

    .then(() => dis.close())
    .then(_.constant(waitEvent(ws, 'message', 3)))
    .then((ret) => JSON.parse(ret[2].toString()))
    .then((ret) => expect(ret).to.contain({ node: null }))

    .then(() => ws.close(1000, 'leaving'))
    .then(_.constant(waitEvent(ws, 'close', 1)))
    .finally(() => dis.close());
  });

  it('fails to query an unknown server', function() {
    var msgs = [];

    /* eslint-disable-next-line max-len */
    var ws = new WebSocket(`http://127.0.0.1:${env.server.address().port}/unknown:1234/dns/query`);
    ws.once('open', () => ws.send(JSON.stringify({ name: '/TEST', sid: 1 })));
    ws.on('message', (data) => msgs.push(data));
    return q()
    .then(_.constant(waitEvent(ws, 'close', 1, 2000)))
    .then((evt) => {
      expect(msgs).to.be.empty();
      expect(evt).to.equal(1001);
      ws.removeAllListeners();
    });
  });

  it('doesn\'t fail when websocket is closed', function(done) {
    var ws = new WebSocket(env.url);
    ws.once('open', () => {
      ws.send(JSON.stringify({ name: 'DIS_DNS/SERVER_LIST', sid: 1 }));
      ws.once('close', () => { ws.removeAllListeners(); done(); });
      ws.close(1000, 'bye');
    });
  });
});
