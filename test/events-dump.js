
const _ = require('lodash');
const { before, after } = require('mocha');
const { stackTraceFilter } = require('mocha/lib/utils');

var EventEmitter = require('events');

try {
  var process = require('process'); /* eslint-disable-line global-require */
}
catch (e) { } /* eslint-disable-line no-empty */

if (_.get(process, 'env.EVENTS_DUMP') === '1') {

  const allListeners = new Map();

  // this will pull Mocha internals out of the stacks
  const filterStack = stackTraceFilter();
  const skip = [ 'Hook', 'WriteStream', 'Console' ];
  const orig = {
    addListener: EventEmitter.prototype.addListener,
    removeListener: EventEmitter.prototype.removeListener,
    removeAllListeners: EventEmitter.prototype.removeAllListeners
  };

  before(function() {

    function addListener(event, cb) {
      var name = _.get(this, 'constructor.name');
      if (!_.includes(skip, name)) {
        var listener = allListeners.get(this);
        if (!listener) {
          listener = {};
          allListeners.set(this, listener);
        }
        var err = new Error();
        if (_.has(listener, event)) {
          listener[event].push({ cb, stack: err.stack });
        }
        else {
          listener[event] = [ { cb, stack: err.stack } ];
        }
      }
      return orig.addListener.call(this, event, cb);
    }

    function removeListener(event, cb) {
      var name = _.get(this, 'constructor.name');
      if (!_.includes(skip, name)) {
        var listener = allListeners.get(this);
        if (listener && _.has(listener, event)) {
          _.remove(listener[event], (rec) => rec.cb === cb);
          if (_.isEmpty(listener[event])) {
            _.unset(listener, event);
          }
        }
        else {
          /* eslint-disable-next-line max-len */
          console.error(`listener not found: ${_.get(this, 'constructor.name', this)}.on("${event}") => ${cb}`);
          console.error(filterStack((new Error()).stack));
        }
        if (_.isEmpty(listener)) {
          allListeners.delete(this);
        }
      }
      return orig.removeListener.call(this, event, cb);
    }

    function removeAllListeners(event) {
      if (event) {
        var listener = allListeners.get(this);
        _.unset(listener, event);
      }
      else {
        allListeners.delete(this);
      }
      return orig.removeAllListeners.call(this, event);
    }

    EventEmitter.prototype.addListener = addListener;
    EventEmitter.prototype.on = addListener;

    EventEmitter.prototype.removeListener = removeListener;
    EventEmitter.prototype.removeAllListeners = removeAllListeners;

  });

  after(function() {
    EventEmitter.prototype.addListener = orig.addListener;
    EventEmitter.prototype.on = orig.addListener;

    EventEmitter.prototype.removeListener = orig.removeListener;
    EventEmitter.prototype.removeAllListeners = orig.removeAllListeners;

    if (!_.isEmpty(allListeners)) {
      var total = 0;
      allListeners.forEach((listeners, key) => {
        var name = _.get(key, 'constructor.name', key);
        if (_.includes(skip, name)) {
          return;
        }

        _.forEach(listeners, (recArray, event) => {
          console.error(`Event: ${name}.on("${event}")`);
          _.forEach(recArray, (rec) => {
            total += 1;
            console.error(`Listener: ${rec.cb}`);
            console.error(filterStack(rec.stack));
          });
          console.error('\n');
        });
      });
      console.error(`Events connected: ${total}`);
    }
  });

}
