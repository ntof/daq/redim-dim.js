# ReDIM to DIM proxy

This library implements a ReDIM to DIM proxy, as described
in [ReDIM specification](https://mro-dev.web.cern.ch/docs/std/ReDIM-protocol.html).

# Usage

To use this library:
```bash
# Install it
npm install "git+https://gitlab.cern.ch/ntof/daq/redim-dim.js.git"
```

To load it on an [Express](https://expressjs.com/) application:
```js
const
  redim = require('@ntof/redim-dim'),
  express = require('express')

const app = express();
redim.register(app);

app.listen(8080)
```

# Examples

Here are some example queries:
```bash
$ curl 'http://localhost:8080/ntofproxy-1.cern.ch/dns/query?name=EACS/State'
# {"task":"EACS","pid":16641,"port":5101,"protocol":1,"format":33,"node":"ntofapp-1.cern.ch","address":2887443837,"definition":"C"}

$ curl 'http://localhost:8080/ntofapp-1.cern.ch:5101/dis/subscribe?name=EACS/State&definition=C'
# {"value":"<?xml version=\"1.0\"?>\n<state value=\"-2\" strValue=\"ERROR\">\n\t<value value=\"-2\" strValue=\"ERROR\" />\n\t<value value=\"-1\" strValue=\"NOT READY\" />\n\t<value value=\"1\" strValue=\"LOADING\" />\n\t<value value=\"2\" strValue=\"IDLE\" />\n\t<value value=\"3\" strValue=\"FILTERS INITIALISATION\" />\n\t<value value=\"4\" strValue=\"SAMPLES INITIALISATION\" />\n\t<value value=\"5\" strValue=\"COLLIMATOR INITIALISATION\" />\n\t<value value=\"6\" strValue=\"HIGHVOLTAGE INITIALISATION\" />\n\t<value value=\"7\" strValue=\"DAQS INITIALISATION\" />\n\t<value value=\"8\" strValue=\"MERGER INITIALISATION\" />\n\t<value value=\"9\" strValue=\"STARTING\" />\n\t<value value=\"10\" strValue=\"RUNNING\" />\n\t<value value=\"11\" strValue=\"STOPPING\" />\n\t<error active=\"true\" code=\"392\">[HV_15/Acquisition]: no link</error>\n\t<errors>\n\t\t<error code=\"392\">[HV_15/Acquisition]: no link</error>\n\t\t<error code=\"380\">[HV_13/Acquisition]: no link</error>\n\t\t<error code=\"368\">[HV_12/Acquisition]: no link</error>\n\t\t<error code=\"356\">[HV_9/Acquisition]: no link</error>\n\t\t<error code=\"344\">[HV_8/Acquisition]: no link</error>\n\t\t<error code=\"332\">[HV_7/Acquisition]: no link</error>\n\t\t<error code=\"320\">[HV_5/Acquisition]: no link</error>\n\t\t<error code=\"312\">[HV_3/Acquisition]: no link</error>\n\t\t<error code=\"300\">[HV_1/Acquisition]: no link</error>\n\t\t<error code=\"1\">[LastError]: [ntofdaq-m6]: no link</error>\n\t</errors>\n\t<warnings />\n</state>\n","name":"EACS/State"}
```

To better test the API a swagger live documentation is available http://localhost:8080/api-docs.

# Tests

To run the unit-tests:
```bash
npm run test
```
