// / <reference types="node" />
/* eslint-disable max-len,max-lines */
import { EventEmitter } from 'events';
import { IRouter, Request } from 'express';
import { packets } from '@cern/dim';
import WebSocket from 'ws';

export = redimDim;
export as namespace redimDim;

declare namespace redimDim {
  export class DisProxy extends EventEmitter {
    constructor(host: string, port: number);

      dis: dim.DisClient;

      unref(): void;

      ref(): void;

      subscribe(params: SubscribeOptions, cb: (subId: string) => any): Promise<RepInfo | null>;

      abort(subId: string | number): void;

      command(params: CmdReq): Promise<any>;

      wsSubscribe(ws: WebSocket, data: any): Promise<boolean | void>;

      wsCommand(ws: WebSocket, data: any): Promise<boolean | void>;

      static serverParams(req: Request): Promise<{ host: string, port: number }>;

      static register(app: IRouter): void;
  }

  export class DnsProxy extends EventEmitter {
    static serverParams(req: Request): Promise<{ host: string, port?: number }>;

    static register(app: IRouter): void;

    constructor(node: string, port?: number | null);

      dns: dim.DnsClient;

      release(): void;

      query(params: Partial<{ name: string, sid: sid }>, cb: (serviceInfo: ServiceInfo) => any): Promise<ServiceInfo | null>;

      wsQuery(ws: WebSocket, data: any): Promise<void>;
  }

  export namespace SwaggerDoc {
    function register(app: IRouter, path?: string): void;
  }

  export function register(app: IRouter, path?: string): void;

  interface RepInfo extends packets.DicRepInfo {
    name: string
    sid: number|string
  }

  interface CmdReq {
    name: string
    sid?: string|number
    data: any
    definition: string
  }

  type sid = number;

  interface SubscribeOptions {
    name: string,
    serviceInfo: string|dim.ServiceInfo,
    watch: boolean,
    timer: number,
    opts: { stamped?: boolean, type?: number, timeout?: number },
    initial: boolean,
    sid: number
  }

  interface LiveServiceInfo {
    node: string;
    task: string;
    address: (number|string|null);
    pid: number;
    port: number;
    protocol: number;
    format: number|null;
    definition: string;
    sid?: sid;
  }
  interface NullServiceInfo { node: null, sid?: sid }
  type ServiceInfo = LiveServiceInfo | NullServiceInfo
}
