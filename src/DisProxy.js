// @ts-check
/* eslint-disable max-lines */
const
  q = require('q'),
  _ = require('./lodash-ext'),

  { DisClient, packets, ServiceInfo, ServiceDefinition } = require('@cern/dim'),
  { EventEmitter } = require('events'),
  express = require('express'),
  cors = require('cors'),
  debug = require('debug')('redim:proxy'),

  { isKeepAlive, parseBool, parseHost } = require('./utils'),
  ProxyError = require('./ProxyError');

/**
 * @typedef {import('ws')} WebSocket
 * @typedef {import('express').Request} Request
 * @typedef {import('express').IRouter} expressIRouter
 * @typedef {import('express-ws').WithWebsocketMethod & expressIRouter} IRouter
 */

/**
 * @param {string} name
 * @param {?(number|string)=} sid
 * @param {dim.packets.DicRepInfo=} ret
 * @return {redimDim.RepInfo}
 */
function _convert(name, sid, ret) {
  /** @type {Partial<redimDim.RepInfo>} */
  var reply = _.pick(ret, [ 'timestamp', 'quality', 'value' ]);
  if (!ret || !reply) {
    // @ts-ignore
    return ret;
  }
  else if (!_.has(reply, 'value')) {
    reply.value = _.isEmpty(ret.data) ? null : ret.data.toString('base64');
  }
  reply.name = name;
  if (sid) { reply.sid = sid; }
  return /** @type {redimDim.RepInfo} */ (reply);
}

/**
 * @param {redimDim.SubscribeOptions} params
 * @return {dim.packets.DicReq.Type}
 */
function _getType(params) {
  /* old protocol doesn't support !watch && !initial
    (timed without first value) */
  if (!params.watch) {
    return packets.DicReq.Type.TIMED_ONLY;
  }
  else if (params.initial) {
    return packets.DicReq.Type.MONITORED;
  }
  else {
    return packets.DicReq.Type.UPDATE;
  }
}

/**
 * @param {any} params
 * @return {redimDim.SubscribeOptions}
 */
function _getSubscribeOpts(params) {
  return {
    name: _.get(params, 'name'),
    serviceInfo: _.has(params, 'definition') ?
      (new ServiceInfo(_.get(params, 'name'), params.definition)) :
      _.get(params, 'name'),
    watch: parseBool(_.get(params, 'watch', false)),
    timer: _.toNumber(_.get(params, 'timer', 0)),
    opts: { stamped: parseBool(_.get(params, 'stamped', false)) },
    initial: parseBool(_.get(params, 'initial', true)),
    sid: _.toNumber(_.get(params, 'sid'))
  };
}

class DisProxy extends EventEmitter {
  /**
   * @param {string} host
   * @param {number} port
   */
  constructor(host, port) {
    super();
    this.dis = new DisClient({ name: host, port: port }, { retryDelay: 0 });
    this._ref = 1;
    /** @type {{ [sid: number]: { subId: number, abort: () => any } }} */
    this._subs = {};
    /** @type {?Q.Promise<any>} */
    this._rpc = null;
  }

  unref() {
    if (--this._ref <= 0) {
      this.removeAllListeners();
      this.dis.close();
    }
  }

  ref() {
    ++this._ref;
  }

  /**
   * @openapi
   * /{server}/dis/subscribe:
   *   get:
   *     tags: ['DIM', 'DIS']
   *     summary: subscribe to a service
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: server
   *         in: path
   *         description: DIM server to connect to
   *       - name: name
   *         in: query
   *         required: true
   *         type: string
   *         description: the service to subscribe to
   *         example: DIS_DNS/SERVER_LIST
   *       - name: sid
   *         in: query
   *         required: true
   *         type: string
   *         description: request identifier
   *       - watch:
   *         name: watch
   *         in: query
   *         type: integer
   *         required: false
   *         description: watch the service for changes
   *         default: 0
   *       - timer:
   *         name: timer
   *         in: query
   *         type: integer
   *         required: false
   *         description: update interval in seconds
   *         default: 0
   *       - initial:
   *         name: initial
   *         in: query
   *         type: integer
   *         required: false
   *         description: send current value on subscription
   *         default: 1
   *       - stamped:
   *         name: stamped
   *         in: query
   *         type: integer
   *         required: false
   *         description: retrieve timestamp information with the value (and quality)
   *         default: 0
   *       - definition:
   *         name: definition
   *         in: query
   *         type: string
   *         required: false
   *         description: service definition value
   */

  /**
   * @param {redimDim.SubscribeOptions} params
   * @param {(subId: string) => any} cb
   * @return {Q.Promise<?redimDim.RepInfo>}
   */
  subscribe(params, cb) {
    return q()
    .then(() => this.dis.connect())
    .catch((err) => {
      debug('failed to connect:', err);
      throw new ProxyError(ProxyError.Code.EHOSTDOWN, 'server not available');
    })
    .then(() => {
      if (!params.name) {
        throw new ProxyError(ProxyError.Code.EINVAL, 'name argument missing');
      }

      if (!params.watch && (params.timer === 0)) { /* no long-polling */
        if (params.initial) {
          debug('service request (one-shot) sid:%i name:%s', params.sid,
            params.name);
          return this.dis.request(params.serviceInfo, params.opts)
          .then((reply) => {
            const ret = _convert(params.name, params.sid, reply);
            if (_.isNil(ret)) {
              throw new ProxyError(ProxyError.Code.EHOSTDOWN,
                'service not available');
            }
            return ret;
          });
        }
        else {
          /* resolving will end the request */
          debug('service unsubscribe sid:%i name:%s', params.sid, params.name);
          const sub = this._subs[params.sid];
          if (_.isNil(sub)) {
            debug('failed to unsubscribe unknown sid', params.sid);
          }
          else {
            this.removeAllListeners(_.toString(sub.subId));
            this.abort(sub.subId);
            sub.abort();
            delete this._subs[params.sid];
          }
        }
        return undefined;
      }
      else {
        params.opts.type = _getType(params);
        params.opts.timeout = params.timer;
        var def = q.defer();

        debug('service request sid:%i name:%s watch:%s timer:%i initial:%s',
          params.sid, params.name, params.watch, params.timer, params.initial);
        var subId = this.dis.monitor(params.serviceInfo, params.opts, (ret) => {
          var value = _convert(params.name, params.sid, ret);
          if (value === undefined) {
            this.abort(subId);
            def.reject(new ProxyError(ProxyError.Code.EHOSTDOWN,
              'service not available'));
          }
          else if (!params.initial && !params.watch) {
            /* TIMED_ONLY doesn't support initial=false, let's emulate */
            params.initial = true;
          }
          else {
            this.emit(_.toString(subId), value);
          }
        });
        this._subs[params.sid] = { subId, abort: () => def.resolve() };
        _.attempt(cb, _.toString(subId));
        return def.promise;
      }
    });
  }

  /**
   * @param  {string|number} subId
   */
  abort(subId) {
    this.dis.abort(_.toNumber(subId));
  }

  /**
   * @openapi
   * /{server}/dis/command:
   *   post:
   *     tags: ['DIM', 'DIS']
   *     summary: send a command to a service
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: server
   *         in: path
   *         description: DIM server to send the command to
   *       - name: command
   *         in: body
   *         schema:
   *           type: object
   *           required: [ name, definition ]
   *           properties:
   *             name:
   *               description: the service to send the command to
   *               type: string
   *               required: true
   *               example: 'TEST'
   *             sid:
   *               type: string
   *               description: the service ID to match requests with replies (when calling an RPC)
   *               example: 1
   *             data:
   *               description: the request payload
   *               example: "<command>test</command>"
   *             definition:
   *               description: the command signature
   *               type: string
   *               required: true
   *               example: 'C'
   */
  /**
   * @param  {redimDim.CmdReq} params
   * @return {Q.Promise<any>}
   */
  command(params) {
    return q()
    .then(() => this.dis.connect())
    .catch((err) => {
      debug('failed to connect:', err);
      throw new ProxyError(ProxyError.Code.EHOSTDOWN, 'server not available');
    })
    .then(() => {
      var name = _.get(params, 'name');
      /** @type {ServiceInfo|string} */
      var serviceInfo;
      var args = _.get(params, 'data');

      if (!name) {
        return q.reject(new ProxyError(ProxyError.Code.EINVAL,
          'name argument missing'));
      }
      serviceInfo = _.has(params, 'definition') ?
        (new ServiceInfo(name, _.get(params, 'definition'))) : name;

      if (_.has(params, 'sid') && name.endsWith('/RpcIn')) { /* rpc mode */
        debug('rpc call name:%s args:%o', name, args);
        var def = q.defer();
        this._rpc = (this._rpc || q()).catch(_.noop) /* make rpcs sequential */
        .then(() => {
          var replyName = name.slice(0, -2) + 'Out';
          var replyInfo;
          if ((serviceInfo instanceof ServiceInfo) &&
              (serviceInfo.definition instanceof ServiceDefinition)) {
            replyInfo = new ServiceInfo(replyName, new ServiceDefinition(
              { type: ServiceDefinition.Type.SRV,
                params: serviceInfo.definition.returns }));
            serviceInfo.definition.type = ServiceDefinition.Type.CMD;
          }
          else {
            replyInfo = name;
          }
          var subId = this.dis.monitor(replyInfo,
            { type: packets.DicReq.Type.UPDATE, timeout: 0 }, (ret) => {
              this.abort(subId);
              def.resolve(_convert(name, _.get(params, 'sid'), ret));
            });
        })
        .then(() => this.dis.cmd(serviceInfo, args))
        .then(() => def.promise);
        return this._rpc;
      }
      else {
        debug('command call name:%s args:%o', name, args);
        return this.dis.cmd(serviceInfo, args);
      }
    });
  }

  /**
   * @param {WebSocket} ws
   * @param {any} data
   * @return {Q.Promise<boolean|void>}
   */
  wsSubscribe(ws, data) {
    return q()
    .then(() => JSON.parse(_.toString(data)))
    //  @ts-ignore
    .then((jsonData) => {
      if (isKeepAlive(jsonData)) { return true; }
      return q()
      .then(() => _getSubscribeOpts(jsonData))
      .then((params) => this.subscribe(params, (subId) => {
        this.on(subId, (data) => {
          try { ws.send(JSON.stringify(data)); }
          catch (err) { debug('ws error:', err); }
        });
      }))
      .then((value) => {
        if (value) {
          ws.send(JSON.stringify(value));
        }
      });
    })
    .catch((err) => ws.close(
      ProxyError.toWebSocketCode(err.code), _.toString(err.message || err)));
  }

  /**
   * @param {WebSocket} ws
   * @param {any} data
   * @return {Q.Promise<boolean|void>}
   */
  wsCommand(ws, data) {
    return q()
    .then(() => JSON.parse(_.toString(data)))
    // @ts-ignore
    .then((jsonData) => {
      if (isKeepAlive(jsonData)) { return true; }
      this.ref();
      return q()
      .then(() => this.command(jsonData))
      .then((value) => {
        if (value) { ws.send(JSON.stringify(value)); }
      });
    })
    .catch((err) => ws.close(
      ProxyError.toWebSocketCode(err.code), _.toString(err.message || err)))
    .finally(this.unref.bind(this));
  }

  /**
   * @param  {Request} req
   * @return {Q.Promise<{ host: string, port: number }>}
   */
  static serverParams(req) {
    var server = parseHost(decodeURIComponent(req.params.server));
    if (!server || !_.get(server, 'port')) {
      return q.reject(new ProxyError(ProxyError.Code.EINVAL,
        'invalid server info:' + decodeURIComponent(req.params.server)));
    }
    /* @ts-ignore checked above */
    return q(server);
  }

  /**
   * @param  {IRouter} app
   */
  static register(app) {
    app.use('/:server/dis/subscribe', cors());
    app.get('/:server/dis/subscribe', function(req, res) {
      res.set('Cache-control', 'no-store');
      return DisProxy.serverParams(req)
      .then(function(server) {
        var proxy = new DisProxy(server.host, server.port);
        res.set('Content-Type', 'application/json; charset=utf-8');
        res.once('finish', () => { proxy.unref(); res.removeAllListeners(); });
        proxy.subscribe(_getSubscribeOpts(req.query), (subId) => {
          proxy.on(subId, (data) => res.write(JSON.stringify(data)));
          res.once('close', () => { proxy.unref(); res.removeAllListeners(); });
        })
        .then(
          (value) => res.send(value ? JSON.stringify(value) : undefined),
          (err) => {
            /*
              when using http-streaming we may not be able to report an error
              anymore
             */
            if (res.headersSent) {
              res.send(undefined);
            }
            else {
              res.status(err.code || 500).send(_.toString(err.message || err));
            }
          });
      });
    });

    app.use('/:server/dis/command', cors(), express.json());
    app.post('/:server/dis/command', function(req, res) {
      res.set('Cache-control', 'no-store');
      return DisProxy.serverParams(req)
      .then(function(server) {
        var proxy = new DisProxy(server.host, server.port);
        return proxy.command(req.body)
        .then((value) => {
          if (value) {
            res.json(value);
          }
          else {
            res.status(204).send();
          }
        })
        .finally(() => proxy.unref());
      })
      .catch((err) => res.status(err.code || 500).send(
        _.toString(err.message || err)));
    });

    if (_.hasIn(app, 'ws')) {
      app.ws('/:server/dis/subscribe', function(ws, req) {
        return DisProxy.serverParams(req)
        .then(function(server) {
          var proxy = new DisProxy(server.host, server.port);
          ws.once('close', () => { proxy.unref(); ws.removeAllListeners(); });
          ws.on('message', proxy.wsSubscribe.bind(proxy, ws));
        })
        .catch((err) => ws.close(ProxyError.toWebSocketCode(err.code),
          _.toString(err.message || err)));
      });

      app.ws('/:server/dis/command', function(ws, req) {
        return DisProxy.serverParams(req)
        .then(function(server) {
          var proxy = new DisProxy(server.host, server.port);
          ws.once('close', () => { proxy.unref(); ws.removeAllListeners(); });
          ws.on('message', proxy.wsCommand.bind(proxy, ws));
        })
        .catch((err) => ws.close(ProxyError.toWebSocketCode(err.code),
          _.toString(err.message || err)));
      });
    }
    else {
      debug('no WebSocket support');
    }
  }
}

module.exports = DisProxy;
