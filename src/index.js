// @ts-check
const
  DnsProxy = require('./DnsProxy'),
  DisProxy = require('./DisProxy'),
  SwaggerDoc = require('./SwaggerDoc');

/**
 * @typedef {import('express').IRouter} expressIRouter
 * @typedef {import('express-ws').WithWebsocketMethod & expressIRouter} IRouter
 */

module.exports = {
  DnsProxy, DisProxy,
  /**
   * @param {IRouter} app
   * @param {string} [path]
   */
  register(app, path) {
    DnsProxy.register(app);
    DisProxy.register(app);
    SwaggerDoc.register(app, path);
  }
};
