import { expectType } from 'tsd';
import { DisProxy, DnsProxy, RepInfo, ServiceInfo } from '.';

import { Router } from 'express';
import { DisClient, DnsClient, SwaggerDoc, register } from '@cern/dim';

/* DIC */
(async function() {
  const app = Router();

  /* this could have been sufficient as an external API */
  DisProxy.register(app);
  DnsProxy.register(app);

  /* slightly deeper */
  const disProxy = new DisProxy('localhost', 1234);
  expectType<RepInfo|null>(await disProxy.subscribe(
    { name: '/test',
      serviceInfo: 'C',
      watch: true,
      timer: 0,
      opts: {},
      initial: true, sid: 42
    }, (subId: string) => { disProxy.abort(subId); }));

  await disProxy.command({
    name: "ntofdaq-m1.cern.ch/Daq/Command/Cmd",
    definition: "C",
    data: "<command key=\"253018630\"><command><command>reset" +
      "</command></command></command>"
  });

  expectType<DisClient>(disProxy.dis);
  disProxy.unref();

  const dnsProxy = new DnsProxy('localhost');
  expectType<ServiceInfo|null>(
    await dnsProxy.query({ name: "/test", sid: 12 },
      (serviceInfo: ServiceInfo) => serviceInfo.sid));

  expectType<DnsClient>(dnsProxy.dns);
  dnsProxy.release();

  SwaggerDoc.register(app);
  SwaggerDoc.register(app, '/test');

  register(app);
  register(app, '/test');
}());
