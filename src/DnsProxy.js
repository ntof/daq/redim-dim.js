// @ts-check
const
  { DnsClient } = require('@cern/dim'),
  q = require('q'),
  _ = require('./lodash-ext'),
  { EventEmitter } = require('events'),
  debug = require('debug')('redim:proxy'),

  { isKeepAlive, parseHost } = require('./utils'),
  cors = require('cors'),
  ProxyError = require('./ProxyError');

/**
 * @typedef {redimDim.sid} sid
 * @typedef {import('ws')} WebSocket
 * @typedef {import('express').Request} Request
 * @typedef {import('express').IRouter} expressIRouter
 * @typedef {import('express-ws').WithWebsocketMethod & expressIRouter} IRouter
 */

/*::
import WebSocket from 'ws'
import type { $Request, $Response, $Application } from 'express'
import type { ServiceInfo } from '@cern/dim'
import type { ex$App, ex$Router } from './utils'

type $ServiceName = string
type $Sid = number

type $QueryParams = $Shape<{
  name: string,
  sid: $Sid
}>

type $ServiceInfo = {
  node: string;
  task: string;
  address: ?(number|string);
  pid: number;
  port: number;
  protocol: number;
  format: ?number;
  definition: string;
  sid?: $Sid;
} | { node: null, sid?: $Sid }
*/

/**
 * @param {dim.ServiceInfo} info
 * @param {?sid=} sid
 * @return {redimDim.ServiceInfo|redimDim.NullServiceInfo}
 */
function _convert(info, sid) {
  /** @type {redimDim.ServiceInfo|redimDim.NullServiceInfo} */
  var ret;
  if (info.node && info.node.isValid()) {
    ret = _.assign(
      _.pick(info.node, [ 'task', 'pid', 'port', 'protocol', 'format' ]),
      {
        node: _.get(info.node, 'name'),
        address: _.get(info.node, 'host'),
        definition: _.toString(info.definition)
      });
  }
  else {
    ret = { node: null };
  }
  if (!_.isNil(sid)) {
    ret.sid = sid;
  }
  return ret;
}

class DnsProxy extends EventEmitter {
  /**
   * @param {string} node
   * @param {?number=} port
   */
  constructor(node, port) {
    super();
    /** @type {Array<Q.Deferred<any>>} */
    this._queries = [];
    /** @type {{ [serviceName: string]: Array<(serviceInfo: dim.ServiceInfo) => any>}} */
    this._watchs = {};
    this.dns = new DnsClient(node, port);
    this.dns.once('close', () => {
      _.forEach(this._queries, (q) => q.resolve());
      this._queries = [];
    });
    this.dns.on('service', (name, info) => {
      _.forEach(this._watchs[name], (cb) => cb(info));
    });
  }

  release() {
    this.removeAllListeners();
    this.dns.close();
    this.dns.removeAllListeners();
  }

  /**
   * @openapi
   * /{server}/dns/query:
   *   get:
   *     tags: ['DIM', 'DNS']
   *     summary: query for a service
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: server
   *         in: path
   *         description: DIM server to connect to
   *       - name: name
   *         in: query
   *         type: string
   *         required: true
   *         example: DIS_DNS/SERVER_LIST
   *       - name: sid
   *         in: query
   *         type: integer
   *         example: 1
   *     responses:
   *       200:
   *         description: service information
   *         examples:
   *           application/json:
   *             task: "DIS_DNS"
   *             pid: 1446
   *             port: 5100
   *             protocol: 1
   *             node: "ntofproxy-2.cern.ch"
   *             address: 2307539281
   *             definition: "C"
   *             sid: "1"
   */

  /**
   * @param {Partial<{ name: string, sid: sid }>} params
   * @param {(serviceInfo: redimDim.ServiceInfo) => any} cb
   * @return {Q.Promise<?redimDim.ServiceInfo>}
   */
  query(params, cb) {
    return q()
    .then(() => this.dns.connect())
    .catch((err) => {
      debug('failed to connect:', err);
      throw new ProxyError(ProxyError.Code.EHOSTDOWN, 'server not available');
    })
    .then(() => {
      var name = _.get(params, 'name');
      if (!name) {
        throw new ProxyError(ProxyError.Code.EINVAL, 'name missing in query');
      }
      else if (_.get(params, 'sid')) {
        const info = this.dns.getServiceInfo(name);
        if (info && info.sid === params.sid) {
          /* refresh request */
          debug('refreshing dns query sid:%i name:%s', params.sid, params.name);
          this.dns.refreshQuery(name);
          return _convert(info, params.sid);
        }

        var subId = _.toString(params.sid);
        var def = q.defer();

        if (cb) {
          var infoCallback = function(/** @type {dim.ServiceInfo} */ info) {
            return cb(_convert(info, params.sid));
          };
          /* $FlowIgnore */
          this._watchs[name] = _.push(this._watchs[name], infoCallback);
        }

        this._queries.push(def);
        debug('dns query name:%s sid:%i', name, subId);
        /* long polling mode */
        return this.dns.query(name)
        .then(() => def.promise);
      }
      else {
        debug('dns query name:%s', name);
        return this.dns.query(name)
        .then((info) => _convert(info, params.sid));
      }
    });
  }

  /**
   * @param  {WebSocket} ws
   * @param {any} data
   * @return {Q.Promise<void>}
   */
  wsQuery(ws, data) {
    return q()
    .then(() => JSON.parse(_.toString(data)))
    // @ts-ignore
    .then((jsonData) => {
      if (isKeepAlive(jsonData)) { return true; }
      return q()
      .then(() => this.query(jsonData, (data) => {
        try { ws.send(JSON.stringify(data)); }
        catch (err) { debug('ws error:', err); }
      }))
      .then((value) => {
        if (value) {
          ws.send(JSON.stringify(value));
        }
        else {
          ws.close(ProxyError.WSCode.NORMAL, 'no such service');
        }
      });
    })
    .catch((err) => ws.close(ProxyError.toWebSocketCode(err.code),
      _.toString(err.message || err)));
  }

  /**
   * @param  {Request} req
   * @return {Q.Promise<{ host: string, port?: number }>}
   */
  static serverParams(req) {
    var server = parseHost(decodeURIComponent(req.params.server));
    if (!server) {
      return q.reject(new ProxyError(ProxyError.Code.EINVAL,
        'invalid server info:' + decodeURIComponent(req.params.server)));
    }
    return q(server);
  }

  /**
   * @param {IRouter} app
   */
  static register(app) {
    app.use('/:server/dns/query', cors());
    app.get('/:server/dns/query', function(req, res) {
      res.set('Cache-control', 'no-store');
      return DnsProxy.serverParams(req)
      .then((server) => {
        var proxy = new DnsProxy(server.host, server.port);
        res.set('Content-Type', 'application/json; charset=utf-8');
        res.once('finish',
          () => { proxy.release(); res.removeAllListeners(); });
        res.once('close',
          () => { proxy.release(); res.removeAllListeners(); });
        return proxy.query(req.query, (data) => {
          res.write(JSON.stringify(data));
        })
        .then(
          (value) => res.send(value ? JSON.stringify(value) : undefined),
          (err) => res.status(err.code || 500).send(
            _.toString(err.message || err)));
      });
    });

    if (_.hasIn(app, 'ws')) {
      app.ws('/:server/dns/query', function(ws, req) {
        return DnsProxy.serverParams(req)
        .then((server) => {
          var proxy = new DnsProxy(server.host, server.port);
          ws.once('close',
            () => { proxy.release(); ws.removeAllListeners('message'); });
          ws.on('message', proxy.wsQuery.bind(proxy, ws));
        })
        .catch((err) => ws.close(ProxyError.toWebSocketCode(err.code),
          _.toString(err.message || err)));
      });
    }
  }
}

module.exports = DnsProxy;
