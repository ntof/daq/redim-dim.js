// @ts-check
const
  _ = require('lodash');

/*::
  export type ex$Req = express$Request
  export type ex$Res = express$Response
  export type ex$App = express$Application<ex$Req, ex$Res>
  export type ex$Router = express$Router<ex$Req, ex$Res>
*/

/**
 * @param  {Partial<{ keepAlive: boolean }>} jsonObj
 * @return {boolean}
 * @details KeepAlive Packet is { keepAlive: true }
 */
function isKeepAlive(jsonObj) {
  return _.hasIn(jsonObj, 'keepAlive');
}

/**
 * @param  {string|boolean} str
 * @return {boolean}
 */
function parseBool(str) {
  if (_.isBoolean(str)) {
    return str;
  }
  str = _.toLower(str);

  return (str !== '0') && (str !== 'false') && (str !== 'off');
}

/**
 * @param  {string} param
 * @return {?{ host: string, port?: number }}
 */
function parseHost(param) {
  var idx = _.lastIndexOf(param, ':');
  if (idx >= 0) {
    var ret = {
      host: param.slice(0, idx),
      port: _.toNumber(param.slice(idx + 1))
    };
    if (!isNaN(ret.port)) {
      return ret;
    }
  }
  return _.isString(param) ? { host: param } : null;
}

module.exports = {
  isKeepAlive, parseBool, parseHost
};
