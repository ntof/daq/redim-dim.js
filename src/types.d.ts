export as namespace redimDim

import '@cern/dim';

export interface RepInfo extends dim.packets.DicRepInfo {
  name: string
  sid: string|number
}

export interface CmdReq {
  name: string
  sid?: string|number
  data: any
  definition: string
}

export type sid = number;

export interface SubscribeOptions {
  name: string,
  serviceInfo: string|dim.ServiceInfo,
  watch: boolean,
  timer: number,
  opts: { stamped?: boolean, type?: number, timeout?: number },
  initial: boolean,
  sid: number
}

export interface LiveServiceInfo {
  node: string;
  task: string;
  address: ?(number|string);
  pid: number;
  port: number;
  protocol: number;
  format: ?number;
  definition: string;
  sid?: sid;
}
export interface NullServiceInfo { node: null, sid?: sid }
export type ServiceInfo = LiveServiceInfo | NullServiceInfo
