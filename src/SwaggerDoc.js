// @ts-check
const
  { mapKeys } = require('lodash'),
  path = require('path'),
  swaggerUi = require('swagger-ui-express'),
  swaggerJSDoc = require('swagger-jsdoc');

/**
 * @typedef {import('express').IRouter} IRouter
 */
module.exports = {
  /**
   * @param {IRouter} router
   * @param {string} [routerPath]
   */
  register(router, routerPath) {
    const doc = swaggerJSDoc({
      definition: {
        info: { title: 'ReDIM-DIM Proxy' }
      },
      apis: [ path.join(__dirname, '**.js') ]
    });

    if (routerPath) {
      doc.paths = mapKeys(doc.paths, (value, p) => path.join(routerPath, p));
    }

    router.get('/api-docs.json', (req, res) => res.json(doc));
    router.use('/api-docs', swaggerUi.serve, swaggerUi.setup(doc,
      { customSiteTitle: 'ReDIM-DIM API-DOCS',
        swaggerOptions: { docExpansion: "none", displayRequestDuration: true } }
    ));
  }
};
