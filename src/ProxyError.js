//@flow
const
  _ = require('lodash');

/** @enum {number} */
const WSCode = {
  NORMAL: 1000,
  GOING_AWAY: 1001,
  PROTOCOL_ERROR: 1002,
  UNSUPPORTED_DATA: 1003,

  // BAD_GATEWAY: 1014, not supported by ws lib, and not documented in RFC
  INTERNAL_ERROR: 1011
};

/** @enum {number} */
const Code = {
  EINVAL: 400,
  EHOSTDOWN: 502
};

class ProxyError extends Error {
  /**
   * @param {Code} code
   * @param {string} message
   */
  constructor(code, message) {
    super(message);
    this.code = code;
  }

  /**
   * @param {Code} code
   * @return {WSCode}
   */
  static toWebSocketCode(code) {
    return _.get(_codeToWsMap, code, ProxyError.WSCode.INTERNAL_ERROR);
  }

}

ProxyError.WSCode = WSCode;
ProxyError.Code = Code;

const _codeToWsMap = {
  [ProxyError.Code.EINVAL]: ProxyError.WSCode.UNSUPPORTED_DATA,
  [ProxyError.Code.EHOSTDOWN]: ProxyError.WSCode.GOING_AWAY
};

module.exports = ProxyError;
